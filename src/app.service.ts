import { Injectable } from '@nestjs/common';
import { Request, Response } from 'express';
import * as fs from 'fs';
import { DnaService } from './dna/dna.service';

@Injectable()
export class AppService {

  /**
   * Execute the main verify function
   * @param req 
   * @param res 
   * @returns 
   */
  async mutationVerify(req: Request, res: Response<any>) {
    try {

      let verifyStatus = this.verifyDNABody(req.body)

      if (verifyStatus) {
        let DNAhasMutation = this.hasMutation(req.body.dna);
        await this.saveDNAtoDB(req.body.dna, DNAhasMutation === true ? DNAhasMutation : false);
        return res.status(DNAhasMutation === true ? 200 : 403).send(DNAhasMutation);

      } else {
        return res.status(400).send(verifyStatus);
      }
    } catch (error) {
      throw new Error(error);
    }


  }

  /**
   *  Main script for know if a person has mutation 
   * @param dna 
   * @returns 
   */
  hasMutation(dna: Array<string>): boolean {
    try {
      let newADN = [];
      dna.forEach((elem: string) => {
        newADN.push(elem.split(''))
      });
      let newADNTransp = newADN[0].map((_, colIndex) => newADN.map(row => row[colIndex]));
      let diag_pr = [];
      let result = false;
      for (let i = 0; i < newADN.length; i++) {
        if (this.equalsChars(newADN[i]) || this.equalsChars(newADNTransp[i])) {
          result = true;
          break
        }

        diag_pr.push(newADN[i][i]);
      }
      return (result || this.equalsChars(diag_pr))
    } catch (error) {
      throw new Error(error);
    }

  }

  /**
   * Obtain equals chars from array
   * @param arr 
   * @param cant 
   * @returns 
   */
  equalsChars(arr: Array<string>, cant = 4): boolean {
    try {
      let value = null;
      let cont = 1;

      arr.forEach(elem => {
        if (cont == cant) {
          return;
        } else if (elem == value) {
          cont++;
        } else {
          value = elem;
          cont = 1
        }
      })
      return (cont == cant);
    } catch (error) {
      throw new Error(error);
    }
  }

  /**
   * Verify schema DNA validations
   * @param body 
   * @returns 
   */
  verifyDNABody(body: { dna: string[] }) {
    try {
      if (!body.dna) {
        return {
          status: 400,
          message: "The body request is not allowed"
        }
      }

      let arrayValid = true;

      for (let i = 0; i < body.dna.length; i++) {
        let strToArr = body.dna[i].split('');

        const hadCharsValid = (value) => (value === 'A') || (value === 'T') || (value === 'C') || (value === 'G');

        if (!strToArr.every(hadCharsValid)) {
          arrayValid = false;
          break;
        }
      }
      if (!arrayValid) {
        return {
          status: 400,
          message: "Only 'A', 'T', 'C' and 'G' are allowed"
        }
      } else {
        return true;
      }
    } catch (error) {
      throw new Error(error);
    }

  }

  /**
   * Save DNA record into DB
   * TODO: Connect function to real DB, for dummy, it works with JSON local file 
   * @param dnaToSave 
   * @param isMutated 
   */
  async saveDNAtoDB(dnaToSave, isMutated: boolean) {
    try {

      let document = {
        ...dnaToSave,
        isMutated
      };
  
      DnaService.prototype.create(document)
    } catch (error) {
      throw new Error(error);
      
    }
  }

  /**
   * Obtain DNA stats 
   * TODO: Connect function to real DB, for dummy, it works with JSON local file 
   * @param req 
   * @param res 
   * @returns 
   */
  async getDNAStats(req: Request, res: Response<any>) {
    try {

      let arrayDNA = await DnaService.prototype.findAll()
      console.log("DNA get test", arrayDNA);

      let count_mutations = 0;
      let count_no_mutations = 0;

      arrayDNA.forEach(elem => {
        if (elem.isMutated === true) count_mutations++; 
        if (elem.isMutated === false) count_no_mutations++; 
      });

      let ratio = count_mutations / count_no_mutations;

      return res.status(200).send({
        count_mutations,
        count_no_mutations,
        ratio
      }); 

    } catch (error) {
      throw new Error(error);
    }
  }

}
