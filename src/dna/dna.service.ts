import { CollectionReference, Firestore } from '@google-cloud/firestore';
import { Inject, Injectable } from '@nestjs/common';

const db = new Firestore({
    keyFilename: './src/env/heroesapp-1c67e-firebase-adminsdk-5vq6i-9582ac7955.json',
    projectId: 'heroesapp-1c67e',
  });

@Injectable()
export class DnaService {
    constructor(
    ) {}

    async create(bodyDNA: any) {

        const docRef = db.collection('dnas').doc();
        await docRef.set(bodyDNA);
        const dnaDoc = await docRef.get();
        const dna = dnaDoc.data();
        return dna;
    }

    async findAll() {
        const snapshot = await db.collection('dnas').get();
        const dnas = [];
        snapshot.forEach(doc => dnas.push(doc.data()));
        console.log("DNAS findAll", dnas)
        return dnas;
    }
}
