import { Controller, Get, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/mutation')
  async hasMutation( @Req() request: Request, @Res() response: Response ) {
    return await this.appService.mutationVerify(request, response);
  }

  @Get('/stats')
  async getStats( @Req() request: Request, @Res() response: Response ) {
    return await this.appService.getDNAStats(request, response);
  }

}
